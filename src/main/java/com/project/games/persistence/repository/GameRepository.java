package com.project.games.persistence.repository;

import com.project.games.persistence.entity.Game;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface GameRepository extends CrudRepository<Game, Integer> {
    boolean existsById(Integer id);
    Optional<Game> findByName(String name);
    Optional<Game> findById(Integer id);
    List<Game> findAll();
    void deleteById(Integer id);
    Game save(Game game);
}
