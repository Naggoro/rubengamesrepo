package com.project.games.persistence.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "games")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "game_id")
    private Integer id;

    private String name;

    @Column(name = "distribution_service")
    private String distributionService;

    @Column(name = "minutes_played")
    private Integer minutesPlayed;

    private Float size;
}
