package com.project.games.controller;

import com.project.games.persistence.entity.Game;
import com.project.games.domain.service.GameService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameService gameService;

    @GetMapping("/{name}")
    @ApiOperation("Search a game by name")
    public ResponseEntity<Game> getGame(@ApiParam(value = "The name of the game", required = true, example = "Hades") @PathVariable("name") String name){
        return gameService.findByName(name).map(game -> new ResponseEntity<>(game,  HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/all")
    @ApiOperation("Get all games")
    public ResponseEntity<List<Game>> getGames(){
        return new ResponseEntity<>(gameService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Delete a game by Id")
    public ResponseEntity deleteGameById(@ApiParam(value = "The Id of the game", required = true, example = "20") @PathVariable("id") int id){
        return gameService.deleteById(id) ?  new ResponseEntity(HttpStatus.OK) :  new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    @ApiOperation("Add a game")
    public ResponseEntity<Game> saveGame(@RequestBody Game game){
        return new ResponseEntity<>(gameService.save(game), HttpStatus.OK);
    }

    @GetMapping("/mostPlayedGame")
    @ApiOperation("Get the most played game")
    public ResponseEntity<Game> getMostPlayedGame(){
        return new ResponseEntity<>(gameService.findMostPlayedGame(), HttpStatus.OK);
    }

    @GetMapping("/totalMinutesPlayed")
    @ApiOperation("Get the total amount of minutes played")
    public ResponseEntity<Integer> getTotalMinutesPlayed(){
        return new ResponseEntity<>(gameService.getTotalMinutesPlayed(), HttpStatus.OK);
    }

    @GetMapping("/downloadTime/{gameId}")
    @ApiOperation("Get a game download time in minutes by game Id")
    public ResponseEntity<Integer> getDownloadTimeById(@ApiParam(value = "The Id of the game", required = true, example = "1") @PathVariable("gameId") int gameId){
        return gameService.getGameDownloadTime(gameId).map(time -> new ResponseEntity<>(time,  HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping()
    @ApiOperation("Update a game")
    public  ResponseEntity<Game> updateGame(@RequestBody Game game){
        return new ResponseEntity<>(gameService.updateGame(game), HttpStatus.OK);
    }
}
