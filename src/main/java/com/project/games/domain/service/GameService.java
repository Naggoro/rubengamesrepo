package com.project.games.domain.service;

import com.project.games.persistence.entity.Game;
import com.project.games.persistence.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class GameService {
    @Autowired
    private GameRepository gameRepository;

    public Optional<Game> findByName(String name){
        return gameRepository.findByName(name);
    }

    public List<Game> findAll(){
        return gameRepository.findAll();
    }

    public Game save(Game game){
        return gameRepository.save(game);
    }

    public boolean existsById (Integer id){
        return gameRepository.existsById(id);
    }

    public boolean deleteById(Integer id){
        if (existsById(id)){
            gameRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Game findMostPlayedGame() {
        return gameRepository.findAll().stream().max(Comparator.comparing(game -> game.getMinutesPlayed())).get();
    }

    public Optional<Integer> getGameDownloadTime(int gameId){
        return gameRepository.findById(gameId).map(game -> calculateDownloadTimeInMinutes(game.getSize())).orElse(Optional.empty());
    }

    public Optional<Integer> calculateDownloadTimeInMinutes(float size){
        int gameSizeInMB = (int) (size*1000);
        int downloadSpeedInMBPerSec = 25;
        int downloadTimeInMinutes = (gameSizeInMB/downloadSpeedInMBPerSec)/60;
        return Optional.of(downloadTimeInMinutes);
    }

    public int getTotalMinutesPlayed(){
        Iterator<Game> itr = gameRepository.findAll().iterator();
        int totalMinutesPlayed = 0;
        while(itr.hasNext()){
            totalMinutesPlayed += itr.next().getMinutesPlayed();
        }
        return totalMinutesPlayed;
    }

    public Game updateGame(Game game){
        gameRepository.save(game);
        return game;
    }
}
